import datetime

import pytest
from django.conf import settings
from rest_framework.test import APIClient

from watchnext.resources.models import Genre, Resource
from watchnext.resources.tests.factories import (
    ResourceFactory, UserFactory,
)


@pytest.fixture
def user():
    return UserFactory()


@pytest.fixture()
def auth_client(user):
    client = APIClient()
    client.force_authenticate(user=user)
    return client


@pytest.fixture()
def movie_resource():
    return ResourceFactory(**{
        'tmdb_id': 550,
        'imdb_id': 'tt0137523',
        'name': 'Fight Club',
        'lang': 'en',
        'date': datetime.date(1999, 10, 15),
        'kind': Resource.Kind.MOVIE,
        'poster_path': '/8kNruSfhk5IoE4eZOc4UpvDn6tq.jpg',
        'genres': [Genre.objects.get(id=6)],
    })


@pytest.fixture()
def serie_resource():
    return ResourceFactory(**{
        'tmdb_id': 550,
        'imdb_id': None,
        'name': 'Peaky Blinders',
        'lang': 'en',
        'date': datetime.date(2013, 9, 12),
        'kind': Resource.Kind.SERIE,
        'poster_path': '/6PX0r5TRRU5y0jZ70y1OtbLYmoD.jpg',
        'genres': Genre.objects.filter(id__in=[4, 6]),
    })


@pytest.fixture()
def tmdb_url():

    def _tmdb_url(endpoint, params=''):
        return (
            f'{settings.BASE_URL}{endpoint}'
            f'?api_key={settings.API_KEY}{params}'
        )

    return _tmdb_url


@pytest.fixture
def movie_find_request(tmdb_url):
    return tmdb_url('/find/tt0137523', '&external_source=imdb_id'), {
        "movie_results": [
            {
                "vote_average": 8.4,
                "overview": "A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground \"fight clubs\" forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion.",
                "release_date": "1999-10-15",
                "adult": False,
                "backdrop_path": "/52AfXWuXCHn3UjD17rBruA9f5qb.jpg",
                "vote_count": 21031,
                "genre_ids": [
                    18
                ],
                "title": "Fight Club",
                "original_language": "en",
                "original_title": "Fight Club",
                "poster_path": "/8kNruSfhk5IoE4eZOc4UpvDn6tq.jpg",
                "id": 550,
                "video": False,
                "popularity": 43.252
            }
        ],
        "person_results": [],
        "tv_results": [],
        "tv_episode_results": [],
        "tv_season_results": []
    }


@pytest.fixture
def serie_find_request(tmdb_url):
    return tmdb_url('/find/tt2442560', '&external_source=imdb_id'), {
        "movie_results": [],
        "person_results": [],
        "tv_results": [
            {
                "backdrop_path": "/wiE9doxiLwq3WCGamDIOb2PqBqc.jpg",
                "id": 550,
                "genre_ids": [
                    18,
                    80
                ],
                "original_language": "en",
                "poster_path": "/6PX0r5TRRU5y0jZ70y1OtbLYmoD.jpg",
                "first_air_date": "2013-09-12",
                "original_name": "Peaky Blinders",
                "origin_country": [
                    "GB"
                ],
                "overview": "A gangster family epic set in 1919 Birmingham, England and centered on a gang who sew razor blades in the peaks of their caps, and their fierce boss Tommy Shelby, who means to move up in the world.",
                "vote_count": 1973,
                "vote_average": 8.6,
                "name": "Peaky Blinders",
                "popularity": 188.54
            }
        ],
        "tv_episode_results": [],
        "tv_season_results": []
    }


@pytest.fixture
def movie_details_request(tmdb_url):
    return tmdb_url('/movie/550'), {
        "adult": False,
        "backdrop_path": "/52AfXWuXCHn3UjD17rBruA9f5qb.jpg",
        "belongs_to_collection": None,
        "budget": 63000000,
        "genres": [
            {
                "id": 18,
                "name": "Drama"
            }
        ],
        "homepage": "http://www.foxmovies.com/movies/fight-club",
        "id": 550,
        "imdb_id": "tt0137523",
        "original_language": "en",
        "original_title": "Fight Club",
        "overview": "A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground \"fight clubs\" forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion.",
        "popularity": 51.342,
        "poster_path": "/8kNruSfhk5IoE4eZOc4UpvDn6tq.jpg",
        "production_companies": [
            {
                "id": 508,
                "logo_path": "/7PzJdsLGlR7oW4J0J5Xcd0pHGRg.png",
                "name": "Regency Enterprises",
                "origin_country": "US"
            },
            {
                "id": 711,
                "logo_path": "/tEiIH5QesdheJmDAqQwvtN60727.png",
                "name": "Fox 2000 Pictures",
                "origin_country": "US"
            },
            {
                "id": 20555,
                "logo_path": "/hD8yEGUBlHOcfHYbujp71vD8gZp.png",
                "name": "Taurus Film",
                "origin_country": "DE"
            },
            {
                "id": 54051,
                "logo_path": None,
                "name": "Atman Entertainment",
                "origin_country": ""
            },
            {
                "id": 54052,
                "logo_path": None,
                "name": "Knickerbocker Films",
                "origin_country": "US"
            },
            {
                "id": 25,
                "logo_path": "/qZCc1lty5FzX30aOCVRBLzaVmcp.png",
                "name": "20th Century Fox",
                "origin_country": "US"
            },
            {
                "id": 4700,
                "logo_path": "/A32wmjrs9Psf4zw0uaixF0GXfxq.png",
                "name": "The Linson Company",
                "origin_country": ""
            }
        ],
        "production_countries": [
            {
                "iso_3166_1": "DE",
                "name": "Germany"
            },
            {
                "iso_3166_1": "US",
                "name": "United States of America"
            }
        ],
        "release_date": "1999-10-15",
        "revenue": 100853753,
        "runtime": 139,
        "spoken_languages": [
            {
                "english_name": "English",
                "iso_639_1": "en",
                "name": "English"
            }
        ],
        "status": "Released",
        "tagline": "Mischief. Mayhem. Soap.",
        "title": "Fight Club",
        "video": False,
        "vote_average": 8.4,
        "vote_count": 21029
    }


@pytest.fixture
def serie_details_request(tmdb_url):
    return tmdb_url('/tv/550'), {
        "backdrop_path": "/wiE9doxiLwq3WCGamDIOb2PqBqc.jpg",
        "created_by": [
            {
                "id": 23227,
                "credit_id": "52fe6452c3a36818d600b632",
                "name": "Steven Knight",
                "gender": 2,
                "profile_path": "/vmM9j2yXI8A3AccX3YgOEZHQWcO.jpg"
            }
        ],
        "episode_run_time": [
            60
        ],
        "first_air_date": "2013-09-12",
        "genres": [
            {
                "id": 18,
                "name": "Drama"
            },
            {
                "id": 80,
                "name": "Crime"
            }
        ],
        "homepage": "http://www.bbc.co.uk/programmes/b045fz8r",
        "id": 550,
        "in_production": True,
        "languages": [
            "en"
        ],
        "last_air_date": "2019-09-22",
        "last_episode_to_air": {
            "air_date": "2019-09-22",
            "episode_number": 6,
            "id": 1921330,
            "name": "Mr Jones",
            "overview": "News of Tommy's activities have reached Winston Churchill. At a family meeting, Tommy lays out his plans for Mosley, which will take place at a rally Sir Oswald is leading. Tommy is surprised by another strategy, coming from an unexpected corner. Meanwhile, Tommy has his own secrets to reveal.",
            "production_code": "",
            "season_number": 5,
            "still_path": "/tK75oU0JfkCAZfC4QWuA1uPDMDg.jpg",
            "vote_average": 8,
            "vote_count": 4
        },
        "name": "Peaky Blinders",
        "next_episode_to_air": None,
        "networks": [
            {
                "name": "BBC One",
                "id": 4,
                "logo_path": "/mVn7xESaTNmjBUyUtGNvDQd3CT1.png",
                "origin_country": "GB"
            },
            {
                "name": "BBC Two",
                "id": 332,
                "logo_path": "/bfAVKGrJGcKTAndYktB7cf8UlBO.png",
                "origin_country": "GB"
            }
        ],
        "number_of_episodes": 30,
        "number_of_seasons": 5,
        "origin_country": [
            "GB"
        ],
        "original_language": "en",
        "original_name": "Peaky Blinders",
        "overview": "A gangster family epic set in 1919 Birmingham, England and centered on a gang who sew razor blades in the peaks of their caps, and their fierce boss Tommy Shelby, who means to move up in the world.",
        "popularity": 188.54,
        "poster_path": "/6PX0r5TRRU5y0jZ70y1OtbLYmoD.jpg",
        "production_companies": [
            {
                "id": 686,
                "logo_path": "/qMVjV8YFbxXrGVF4wD6hRjllVbr.png",
                "name": "Tiger Aspect Productions",
                "origin_country": "GB"
            },
            {
                "id": 80893,
                "logo_path": "/8rITuQjEtHEjdyU49sv9fKBYf8E.png",
                "name": "BBC Studios",
                "origin_country": "GB"
            },
            {
                "id": 20047,
                "logo_path": "/cBjeG0zicn6oTV9NNtEGmmZhEXp.png",
                "name": "Caryn Mandabach Productions",
                "origin_country": "GB"
            },
            {
                "id": 2690,
                "logo_path": "/9XXpd39mDfkWmcNB6R0tpvzo781.png",
                "name": "Screen Yorkshire",
                "origin_country": "GB"
            }
        ],
        "production_countries": [
            {
                "iso_3166_1": "GB",
                "name": "United Kingdom"
            }
        ],
        "seasons": [
            {
                "air_date": None,
                "episode_count": 18,
                "id": 178069,
                "name": "Specials",
                "overview": "",
                "poster_path": "/o5VrKdfN5Xg2pRnQVSqBCi4iNG9.jpg",
                "season_number": 0
            },
            {
                "air_date": "2013-09-12",
                "episode_count": 6,
                "id": 59914,
                "name": "Series 1",
                "overview": "",
                "poster_path": "/yMvESLkgmtdAseh6LN97Ex92vvI.jpg",
                "season_number": 1
            },
            {
                "air_date": "2014-10-02",
                "episode_count": 6,
                "id": 62804,
                "name": "Series 2",
                "overview": "As the 1920s begin to roar, business is booming for the Peaky Blinders gang. Shelby sets his sights on wider horizons and his meteoric rise brings him into contact with both the upper echelons of society and astonishing new adversaries from London’s criminal world. All will test him to the core, though in very different ways. Meanwhile, Shelby’s home turf faces new challenges as an enemy from his past returns to the city with plans for a revenge of biblical proportions.",
                "poster_path": "/8vp08uElPn5S2jHSVuAjrytJiuH.jpg",
                "season_number": 2
            },
            {
                "air_date": "2016-05-04",
                "episode_count": 6,
                "id": 72707,
                "name": "Series 3",
                "overview": "The action has moved on two years to 1924 and as Tommy starts married life he is more determined than ever to go legitimate and keep his family safe. But he finds himself pulled into a web of intrigue more lethal than anything he has yet encountered.",
                "poster_path": "/qiKEvlPNZjo5jawyrHFAYyniUu4.jpg",
                "season_number": 3
            },
            {
                "air_date": "2017-11-14",
                "episode_count": 6,
                "id": 94653,
                "name": "Series 4",
                "overview": "It is 1924, Tommy Shelby is caught in a dangerous web of international intrigue.",
                "poster_path": "/zBAPK0FY2MHuN6pvCQwO9kk16Ol.jpg",
                "season_number": 4
            },
            {
                "air_date": "2019-08-24",
                "episode_count": 6,
                "id": 129442,
                "name": "Series 5",
                "overview": "It is 1929, Tommy Shelby MP is approached by a charismatic politician with a bold vision for Britain, he realises that his response will affect not just his family’s future but that of the entire nation.",
                "poster_path": "/arlFd2cGgP9kvEnL9FwXlcTm28D.jpg",
                "season_number": 5
            }
        ],
        "spoken_languages": [
            {
                "english_name": "English",
                "iso_639_1": "en",
                "name": "English"
            }
        ],
        "status": "Returning Series",
        "tagline": "London's for the taking",
        "type": "Scripted",
        "vote_average": 8.6,
        "vote_count": 1986
    }


@pytest.fixture
def movie_recommendations_results():
    return [
        {
            "genre_ids": [
                80,
                9648,
                53
            ],
            "original_language": "en",
            "original_title": "Se7en",
            "poster_path": "/6yoghtyTpznpBik8EngEmJskVUO.jpg",
            "video": False,
            "vote_average": 8.3,
            "overview": "Two homicide detectives are on a desperate hunt for a serial killer whose crimes are based on the \"seven deadly sins\" in this dark and haunting film that takes viewers from the tortured remains of one victim to the next. The seasoned Det. Sommerset researches each sin in an effort to get inside the killer's mind, while his novice partner, Mills, scoffs at his efforts to unravel the case.",
            "release_date": "1995-09-22",
            "vote_count": 14607,
            "title": "Se7en",
            "adult": False,
            "backdrop_path": "/i5H7zusQGsysGQ8i6P361Vnr0n2.jpg",
            "id": 807,
            "popularity": 42.502
        },
        {
            "genre_ids": [
                53,
                80
            ],
            "original_language": "en",
            "original_title": "Pulp Fiction",
            "poster_path": "/plnlrtBUULT0rh3Xsjmpubiso3L.jpg",
            "video": False,
            "vote_average": 8.5,
            "overview": "A burger-loving hit man, his philosophical partner, a drug-addled gangster's moll and a washed-up boxer converge in this sprawling, comedic crime caper. Their adventures unfurl in three stories that ingeniously trip back and forth in time.",
            "id": 680,
            "vote_count": 20491,
            "title": "Pulp Fiction",
            "adult": False,
            "backdrop_path": "/w7RDIgQM6bLT7JXtH4iUQd3Iwxm.jpg",
            "release_date": "1994-09-10",
            "popularity": 56.239
        },
        {
            "adult": False,
            "backdrop_path": "/q2CtXYjp9IlnfBcPktNkBPsuAEO.jpg",
            "genre_ids": [
                9648,
                53
            ],
            "original_language": "en",
            "original_title": "Memento",
            "poster_path": "/yuNs09hvpHVU1cBTCAk9zxsL2oW.jpg",
            "video": False,
            "id": 77,
            "vote_count": 10596,
            "overview": "Leonard Shelby is tracking down the man who raped and murdered his wife. The difficulty of locating his wife's killer, however, is compounded by the fact that he suffers from a rare, untreatable form of short-term memory loss. Although he can recall details of life before his accident, Leonard cannot remember what happened fifteen minutes ago, where he's going, or why.",
            "release_date": "2000-10-11",
            "vote_average": 8.2,
            "title": "Memento",
            "popularity": 26.72
        },
    ]


@pytest.fixture
def movie_recommendations_single_page_request(tmdb_url, movie_recommendations_results):
    return tmdb_url('/movie/550/recommendations', '&page=1'), {
        "page": 1,
        "results": movie_recommendations_results[:],
        "total_pages": 1,
        "total_results": 3
    }


@pytest.fixture
def movie_recommendations_multiple_pages_request(tmdb_url, movie_recommendations_results):
    return [
        (tmdb_url('/movie/550/recommendations', '&page=1'), {
            "page": 1,
            "results": movie_recommendations_results[:2],
            "total_pages": 3,
            "total_results": 3
        }),
        (tmdb_url('/movie/550/recommendations', '&page=2'), {
            "page": 2,
            "results": movie_recommendations_results[2:],
            "total_pages": 3,
            "total_results": 3
        }),
        (tmdb_url('/movie/550/recommendations', '&page=3'), {
            "page": 3,
            "results": [],
            "total_pages": 3,
            "total_results": 3
        }),
    ]


def make_recommendations_single_page_request():

    def _make_recommendations_single_page_request(tmdb_id, results):
        return tmdb_url(f'/movie/{tmdb_id}/recommendations', '&page=1'), {
            "page": 1,
            "results": results,
            "total_pages": 1,
            "total_results": 3
        }

    return _make_recommendations_single_page_request
