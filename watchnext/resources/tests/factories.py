import datetime

import factory
from django.conf import settings

from watchnext.resources.models import (
    Genre, Item, NextList, Resource, Viewing
)


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = settings.AUTH_USER_MODEL
        django_get_or_create = ('username',)

    username = 'test'


class ResourceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Resource
        django_get_or_create = ('name', 'kind')

    tmdb_id = factory.Sequence(lambda n: n)
    imdb_id = factory.Sequence(lambda n: f'tt{n}')
    name = 'Name'
    lang = 'lang'
    date = datetime.date(2020, 1, 1)
    kind = Resource.Kind.MOVIE
    poster_path = factory.LazyAttribute(
        lambda obj: f'/{obj.name.lower()}.jpg'
    )

    @factory.post_generation
    def genres(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for genre in extracted:
                self.genres.add(genre)
        else:
            self.genres.add(Genre.objects.get(tmdb_id=18))


class ViewingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Viewing

    user = factory.SubFactory(UserFactory)
    resource = factory.SubFactory(ResourceFactory)
    status = Viewing.Status.ENJOYED


class NextListFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = NextList
        django_get_or_create = ('name',)

    name = 'nexts'
    user = factory.SubFactory(UserFactory)


class ItemFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Item

    resource = factory.SubFactory(ResourceFactory)
    next_list = factory.SubFactory(NextListFactory)
    origin = Item.Origin.NEXT
    weight = 0
    score = 0
