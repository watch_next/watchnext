import datetime

import pytest
import responses

from watchnext.resources.models import Item, Resource
from watchnext.resources.utils import NextListReloader, ResourceRetriever
from watchnext.resources.tests.factories import (
    ItemFactory, NextListFactory, ResourceFactory,
    ViewingFactory, UserFactory,
)


@pytest.mark.django_db
class TestRecommendations:

    @responses.activate
    def test_single_page(
        self, movie_resource, movie_recommendations_single_page_request,
    ):
        ResourceFactory(tmdb_id=807)
        url, json = movie_recommendations_single_page_request
        responses.add('GET', url, json=json)
        ResourceRetriever.recommendations(movie_resource)

        assert len(responses.calls) == 1
        self.assert_resource_instances(movie_resource)

    @responses.activate
    def test_multiple_pages(
        self, movie_resource, movie_recommendations_multiple_pages_request,
    ):
        ResourceFactory(tmdb_id=807)
        for (url, json) in movie_recommendations_multiple_pages_request:
            responses.add('GET', url, json=json)
        ResourceRetriever.recommendations(movie_resource)

        assert len(responses.calls) == 3
        self.assert_resource_instances(movie_resource)

    def assert_resource_instances(self, resource):
        assert Resource.objects.count() == 4
        assert resource.recommendations.count() == 3

        reco = Resource.objects.get(tmdb_id=807)
        assert reco.imdb_id is None
        assert reco.name == 'Se7en'
        assert reco.lang == 'en'
        assert reco.date == datetime.date(1995, 9, 22)
        assert reco.kind == Resource.Kind.MOVIE
        assert reco.poster_path == '/6yoghtyTpznpBik8EngEmJskVUO.jpg'
        assert list(reco.recommended_by.all()) == [resource]

        reco = Resource.objects.get(tmdb_id=680)
        assert reco.imdb_id is None
        assert reco.name == 'Pulp Fiction'
        assert reco.lang == 'en'
        assert reco.date == datetime.date(1994, 9, 10)
        assert reco.kind == Resource.Kind.MOVIE
        assert reco.poster_path == '/plnlrtBUULT0rh3Xsjmpubiso3L.jpg'
        assert list(reco.recommended_by.all()) == [resource]

        reco = Resource.objects.get(tmdb_id=77)
        assert reco.imdb_id is None
        assert reco.name == 'Memento'
        assert reco.lang == 'en'
        assert reco.date == datetime.date(2000, 10, 11)
        assert reco.kind == Resource.Kind.MOVIE
        assert reco.poster_path == '/yuNs09hvpHVU1cBTCAk9zxsL2oW.jpg'
        assert list(reco.recommended_by.all()) == [resource]


@pytest.mark.django_db
class TestNextListReloader():

    def test_picks(self):
        ia = ItemFactory(resource__name='A', origin=Item.Origin.PICK)
        ib = ItemFactory(resource__name='B', origin=Item.Origin.PICK)
        ic = ItemFactory(resource__name='C')
        id = ItemFactory(resource__name='D')
        re = ResourceFactory(name='E')
        ResourceFactory(name='F')
        picks_uuids = [ia.resource.uuid, ic.resource.uuid, re.uuid]
        picks = Resource.objects.filter(uuid__in=picks_uuids)
        next_list = NextListReloader.picks(ia.next_list, picks)

        assert {p.resource for p in next_list.picks} == {
            ia.resource, ib.resource, ic.resource, re
        }
        assert {n.resource for n in next_list.nexts} == {
            id.resource
        }

    @responses.activate
    def test_nexts(self, tmdb_url):
        nl = NextListFactory()
        ia = ItemFactory(resource__name='A', origin=Item.Origin.PICK)
        ib = ItemFactory(resource__name='B', origin=Item.Origin.PICK)
        ic = ItemFactory(resource__name='C')
        rd = ResourceFactory(name='D')
        ig = ItemFactory(resource__name='G', origin=Item.Origin.PICK)
        ih = ItemFactory(resource__name='H', origin=Item.Origin.PICK)
        ri = ResourceFactory(name='I')
        ia.resource.recommendations.add(
            ib.resource, ic.resource, rd, ih.resource
        )
        ig.resource.recommendations.add(ia.resource)
        ih.resource.recommendations.add(ia.resource)
        ri.recommendations.add(ia.resource)
        url = tmdb_url(f'/movie/{ib.resource.tmdb_id}/recommendations', '&page=1')
        json = {
            "page": 1,
            "results": [
                {
                    "genre_ids": [18],
                    "original_language": "en",
                    "title": "A",
                    "poster_path": "/a.jpg",
                    "release_date": "2020-09-22",
                    "id": ia.resource.tmdb_id,
                },
                {
                    "genre_ids": [18],
                    "original_language": "en",
                    "title": "C",
                    "poster_path": "/c.jpg",
                    "release_date": "2020-09-22",
                    "id": ic.resource.tmdb_id,
                },
                {
                    "genre_ids": [18],
                    "original_language": "en",
                    "title": "E",
                    "poster_path": "/e.jpg",
                    "release_date": "2020-09-22",
                    "id": 101,
                },
            ],
            "total_pages": 1,
            "total_results": 3
        }
        responses.add('GET', url, json=json)
        NextListReloader.nexts(nl)

        assert set(nl.picks) == {ia, ib, ig, ih}
        assert set(nl.nexts) == {
            ic, Item.objects.get(resource__name='D'),
            Item.objects.get(resource__name='E')
        }

        results = {
            'A': {'score': 5, 'weight': 4},
            'B': {'score': 4, 'weight': 2},
            'C': {'score': 6, 'weight': 0},
            'D': {'score': 4, 'weight': 0},
            'E': {'score': 2, 'weight': 0},
            'G': {'score': 0, 'weight': 1},
            'H': {'score': 4, 'weight': 2},
        }
        for key, vals in results.items():
            item = Item.objects.get(resource__name=key)
            assert item.score == vals['score']
            assert item.weight == vals['weight']
