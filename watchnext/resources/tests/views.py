import datetime
import uuid

import pytest
import responses
from rest_framework.test import APIClient

from watchnext.resources.models import Item, NextList, Resource, Viewing
from watchnext.resources.tests.factories import (
    ItemFactory, NextListFactory, ResourceFactory,
    ViewingFactory, UserFactory,
)


@pytest.mark.django_db
class TestResourceList():

    def test_anonymous_user(self):
        ResourceFactory()
        serie = ResourceFactory(kind=Resource.Kind.SERIE)
        response = APIClient().get('/watchnext/series/')

        assert response.status_code == 200
        assert response.json() == [
            {
                'uuid': str(serie.uuid),
                'name': 'Name',
                'year': 2020,
                'poster_path': '/name.jpg',
                'viewing_status': Viewing.Status.NOTSEEN,
            }
        ]

    def test_authenticated_user(self, auth_client):
        ResourceFactory(kind=Resource.Kind.SERIE)
        va = ViewingFactory(resource__name='A')
        rb = ResourceFactory(name='B')
        response = auth_client.get('/watchnext/movies/')

        assert response.status_code == 200
        assert response.json() == [
            {
                'uuid': str(va.resource.uuid),
                'name': 'A',
                'year': 2020,
                'poster_path': '/a.jpg',
                'viewing_status': Viewing.Status.ENJOYED,
            },
            {
                'uuid': str(rb.uuid),
                'name': 'B',
                'year': 2020,
                'poster_path': '/b.jpg',
                'viewing_status': Viewing.Status.NOTSEEN,
            }
        ]


@pytest.mark.django_db
class TestResourceViewSetRetrieve():

    @pytest.fixture
    def resource_with_recommendations(self):
        date = datetime.date(1999, 10, 15)
        resource = ResourceFactory(date=date)
        recommendations = [
            ResourceFactory(name='A'),
            ResourceFactory(name='B'),
            ResourceFactory(name='C'),
        ]

        resource.recommendations.add(*recommendations)
        resource.save()

        recommendations[0].recommendations.add(resource)
        recommendations[0].save()

        url = f'/watchnext/resources/{resource.uuid}/'
        return url, resource, recommendations

    def test_anonymous_user(self, resource_with_recommendations):
        url, resource, recommendations = resource_with_recommendations
        response = APIClient().get(url)

        assert response.status_code == 200
        assert response.json() == {
            'uuid': str(resource.uuid),
            'name': 'Name',
            'date': '1999-10-15',
            'poster_path': '/name.jpg',
            'viewing_status': Viewing.Status.NOTSEEN,
            'genres': [
                {'id': 6, 'name': 'Drama'},
            ],
            'recommended_by': [
                {
                    'uuid': str(recommendations[0].uuid),
                    'name': 'A',
                    'year': 2020,
                    'poster_path': '/a.jpg',
                    'viewing_status': Viewing.Status.NOTSEEN,
                }
            ],
            'recommendations': [
                {
                    'uuid': str(recommendations[0].uuid),
                    'name': 'A',
                    'year': 2020,
                    'poster_path': '/a.jpg',
                    'viewing_status': Viewing.Status.NOTSEEN,
                },
                {
                    'uuid': str(recommendations[1].uuid),
                    'name': 'B',
                    'year': 2020,
                    'poster_path': '/b.jpg',
                    'viewing_status': Viewing.Status.NOTSEEN,
                },
                {
                    'uuid': str(recommendations[2].uuid),
                    'name': 'C',
                    'year': 2020,
                    'poster_path': '/c.jpg',
                    'viewing_status': Viewing.Status.NOTSEEN,
                }
            ]
        }

    def test_authenticated_user(self, auth_client, resource_with_recommendations):
        url, resource, recommendations = resource_with_recommendations
        ViewingFactory(resource=resource)
        ViewingFactory(
            resource=recommendations[0],
            status=Viewing.Status.NEITHER,
        )
        response = auth_client.get(url)

        assert response.status_code == 200
        assert response.json() == {
            'uuid': str(resource.uuid),
            'name': 'Name',
            'date': '1999-10-15',
            'poster_path': '/name.jpg',
            'viewing_status': Viewing.Status.ENJOYED,
            'genres': [
                {'id': 6, 'name': 'Drama'},
            ],
            'recommended_by': [
                {
                    'uuid': str(recommendations[0].uuid),
                    'name': 'A',
                    'year': 2020,
                    'poster_path': '/a.jpg',
                    'viewing_status': Viewing.Status.NEITHER,
                }
            ],
            'recommendations': [
                {
                    'uuid': str(recommendations[0].uuid),
                    'name': 'A',
                    'year': 2020,
                    'poster_path': '/a.jpg',
                    'viewing_status': Viewing.Status.NEITHER,
                },
                {
                    'uuid': str(recommendations[1].uuid),
                    'name': 'B',
                    'year': 2020,
                    'poster_path': '/b.jpg',
                    'viewing_status': Viewing.Status.NOTSEEN,
                },
                {
                    'uuid': str(recommendations[2].uuid),
                    'name': 'C',
                    'year': 2020,
                    'poster_path': '/c.jpg',
                    'viewing_status': Viewing.Status.NOTSEEN,
                }
            ]
        }


@pytest.mark.django_db
class TestResourceViewSetViewing():

    def test_anonymous_user(self):
        url = f'/watchnext/resources/{ResourceFactory().uuid}/viewing/'
        data = {'status': Viewing.Status.ENJOYED}
        response = APIClient().post(url, data=data)

        assert response.status_code == 403

    def test_create_viewing(self, user, auth_client):
        resource = ResourceFactory()
        url = f'/watchnext/resources/{resource.uuid}/viewing/'
        data = {'status': Viewing.Status.ENJOYED}
        response = auth_client.post(url, data=data)

        assert response.status_code == 200, response.json()
        assert user.viewings.count() == 1

        viewing = user.viewings.first()
        assert viewing.resource == resource
        assert viewing.status == Viewing.Status.ENJOYED

    def test_update_viewing(self, user, auth_client):
        viewing = ViewingFactory()
        url = f'/watchnext/resources/{viewing.resource.uuid}/viewing/'
        data = {'status': Viewing.Status.UNLIKED}
        response = auth_client.post(url, data=data)

        assert response.status_code == 200, response.json()
        assert user.viewings.count() == 1

        viewing = user.viewings.first()
        assert viewing.resource == viewing.resource
        assert viewing.status == Viewing.Status.UNLIKED

    @pytest.mark.parametrize('data', [{}, {'status': -1}])
    def test_invalid_data(self, user, auth_client, data):
        resource = ResourceFactory()
        url = f'/watchnext/resources/{resource.uuid}/viewing/'
        response = auth_client.post(url, data=data)

        assert response.status_code == 400
        assert user.viewings.count() == 0


@pytest.mark.django_db
class TestNextListViewSetList():
    url = '/watchnext/next_lists/'

    def test_anonymous_user(self):
        response = APIClient().get(self.url)

        assert response.status_code == 403

    def test_without_data(self, auth_client):
        response = auth_client.get(self.url)

        assert response.status_code == 200
        assert response.json() == []

    def test_with_data(self, auth_client):
        ItemFactory(resource__name='A', score=1)
        ItemFactory(resource__name='C', score=3, origin=Item.Origin.PICK)
        ib = ItemFactory(resource__name='B', score=2)
        el = NextListFactory(name='empty')
        response = auth_client.get(self.url)

        assert response.status_code == 200
        assert response.json() == [
            {
                'uuid': str(ib.next_list.uuid),
                'name': 'nexts',
                'next': {
                    'score': 2,
                    'weight': 0,
                    'origin': Item.Origin.NEXT,
                    'resource': {
                        'uuid': str(ib.resource.uuid),
                        'name': 'B',
                        'year': 2020,
                        'poster_path': '/b.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                }
            },
            {
                'uuid': str(el.uuid),
                'name': 'empty',
                'next': None,
            }
        ]


@pytest.mark.django_db
class TestNextListViewSetCreate():
    url, data = '/watchnext/next_lists/', {'name': 'nexts'}

    def test_anonymous_user(self):
        response = APIClient().post(self.url, data=self.data)

        assert response.status_code == 403

    def test_valid_data(self, user, auth_client):
        response = auth_client.post(self.url, data=self.data)

        assert response.status_code == 201, response.json()
        assert user.next_lists.count() == 1

        next_list = user.next_lists.first()
        assert next_list.name == 'nexts'

    @pytest.mark.parametrize('data', [{}, {'fail': 'nexts'}])
    def test_invalid_data(self, user, auth_client, data):
        response = auth_client.post(self.url, data=data)

        assert response.status_code == 400
        assert user.next_lists.count() == 0


@pytest.mark.django_db
class TestNextListViewSetRetrieve():

    def test_anonymous_user(self):
        next_list = NextListFactory()
        url = f'/watchnext/next_lists/{next_list.uuid}/'
        response = APIClient().get(url)

        assert response.status_code == 403

    def test_other_user(self, auth_client):
        other_user = UserFactory(username='other')
        next_list = NextListFactory(user=other_user)
        url = f'/watchnext/next_lists/{next_list.uuid}/'
        response = auth_client.get(url)

        assert response.status_code == 404

    def test_without_data(self, auth_client):
        empty_list = NextListFactory(name='empty')
        url = f'/watchnext/next_lists/{empty_list.uuid}/'
        response = auth_client.get(url)

        assert response.status_code == 200
        assert response.json() == {
            'uuid': str(empty_list.uuid),
            'name': 'empty',
            'next': None,
            'nexts': [],
            'picks': [],
        }

    def test_with_data(self, auth_client):
        ia = ItemFactory(resource__name='A', score=1)
        ib = ItemFactory(resource__name='B', score=2)
        ic = ItemFactory(
            resource__name='C', score=3, weight=1,
            origin=Item.Origin.PICK,
        )
        ViewingFactory(resource=ic.resource)
        url = f'/watchnext/next_lists/{ia.next_list.uuid}/'
        response = auth_client.get(url)

        assert response.status_code == 200
        assert response.json() == {
            'uuid': str(ia.next_list.uuid),
            'name': 'nexts',
            'next': {
                'score': 2,
                'weight': 0,
                'origin': Item.Origin.NEXT,
                'resource': {
                    'uuid': str(ib.resource.uuid),
                    'name': 'B',
                    'year': 2020,
                    'poster_path': '/b.jpg',
                    'viewing_status': Viewing.Status.NOTSEEN,
                }
            },
            'nexts': [
                {
                    'score': 2,
                    'weight': 0,
                    'origin': Item.Origin.NEXT,
                    'resource': {
                        'uuid': str(ib.resource.uuid),
                        'name': 'B',
                        'year': 2020,
                        'poster_path': '/b.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 1,
                    'weight': 0,
                    'origin': Item.Origin.NEXT,
                    'resource': {
                        'uuid': str(ia.resource.uuid),
                        'name': 'A',
                        'year': 2020,
                        'poster_path': '/a.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
            ],
            'picks': [
                {
                    'score': 3,
                    'weight': 1,
                    'origin': Item.Origin.PICK,
                    'resource': {
                        'uuid': str(ic.resource.uuid),
                        'name': 'C',
                        'year': 2020,
                        'poster_path': '/c.jpg',
                        'viewing_status': Viewing.Status.ENJOYED,
                    }
                },
            ],
        }


@pytest.mark.django_db
class TestNextListViewSetUpdate():

    def test_anonymous_user(self):
        next_list = NextListFactory()
        url = f'/watchnext/next_lists/{next_list.uuid}/'
        response = APIClient().put(url, data={'name': 'updates'})

        assert response.status_code == 403

    def test_other_user(self, auth_client):
        other_user = UserFactory(username='other')
        next_list = NextListFactory(user=other_user)
        url = f'/watchnext/next_lists/{next_list.uuid}/'
        response = auth_client.put(url, data={'name': 'updates'})

        assert response.status_code == 404

    def test_valid_data(self, user, auth_client):
        empty_list = NextListFactory(name='empty')
        url = f'/watchnext/next_lists/{empty_list.uuid}/'
        response = auth_client.put(url, data={'name': 'updates'})

        assert response.status_code == 200, response.json()
        assert user.next_lists.count() == 1

        next_list = user.next_lists.first()
        assert next_list.name == 'updates'

    @pytest.mark.parametrize('data', [{}, {'name': None}])
    def test_invalid_data(self, user, auth_client, data):
        empty_list = NextListFactory(name='empty')
        url = f'/watchnext/next_lists/{empty_list.uuid}/'
        response = auth_client.put(url, data=data)

        assert response.status_code == 400, response.json()
        assert user.next_lists.count() == 1

        next_list = user.next_lists.first()
        assert next_list.name == 'empty'


@pytest.mark.django_db
class TestNextListViewSetDelete():

    def test_anonymous_user(self):
        next_list = NextListFactory()
        url = f'/watchnext/next_lists/{next_list.uuid}/'
        response = APIClient().delete(url)

        assert response.status_code == 403

    def test_other_user(self, auth_client):
        other_user = UserFactory(username='other')
        next_list = NextListFactory(user=other_user)
        url = f'/watchnext/next_lists/{next_list.uuid}/'
        response = auth_client.delete(url)

        assert response.status_code == 404

    def test_without_data(self, user, auth_client):
        empty_list = NextListFactory(name='empty')
        url = f'/watchnext/next_lists/{empty_list.uuid}/'
        response = auth_client.delete(url)

        assert response.status_code == 204
        assert user.next_lists.count() == 0


@pytest.mark.django_db
class TestNextListViewSetPickResource():

    def test_anonymous_user(self):
        next_list = NextListFactory()
        url = f'/watchnext/next_lists/{next_list.uuid}/pick_resource/'
        data = {'uuids': [uuid.uuid4()]}
        response = APIClient().post(url, data=data)

        assert response.status_code == 403

    def test_other_user(self, auth_client):
        other_user = UserFactory(username='other')
        next_list = NextListFactory(user=other_user)
        url = f'/watchnext/next_lists/{next_list.uuid}/pick_resource/'
        data = {'uuids': [uuid.uuid4()]}
        response = auth_client.post(url, data=data)

        assert response.status_code == 404

    @responses.activate
    def test_valid_data(self, auth_client, tmdb_url):
        next_list = NextListFactory()
        ra = ResourceFactory(name='A')
        rb = ResourceFactory(name='B')
        rc = ResourceFactory(name='C')
        rd = ResourceFactory(name='D')
        rg = ResourceFactory(name='G')
        rh = ResourceFactory(name='H')
        ri = ResourceFactory(name='I')
        ra.recommendations.add(rb, rc, rd, rh)
        rg.recommendations.add(ra)
        rh.recommendations.add(ra)
        ri.recommendations.add(ra)
        url = tmdb_url(f'/movie/{rb.tmdb_id}/recommendations', '&page=1')
        json = {
            "page": 1,
            "results": [
                {
                    "genre_ids": [18],
                    "original_language": "en",
                    "title": "A",
                    "poster_path": "/a.jpg",
                    "release_date": "2020-09-22",
                    "id": ra.tmdb_id,
                },
                {
                    "genre_ids": [18],
                    "original_language": "en",
                    "title": "C",
                    "poster_path": "/c.jpg",
                    "release_date": "2020-09-22",
                    "id": rc.tmdb_id,
                },
                {
                    "genre_ids": [18],
                    "original_language": "en",
                    "title": "E",
                    "poster_path": "/e.jpg",
                    "release_date": "2020-09-22",
                    "id": 101,
                },
            ],
            "total_pages": 1,
            "total_results": 3
        }
        responses.add('GET', url, json=json)
        url = f'/watchnext/next_lists/{next_list.uuid}/pick_resource/'
        data = {'uuids': [ra.uuid, rb.uuid, rg.uuid, rh.uuid]}
        response = auth_client.post(url, data=data)

        assert response.status_code == 200
        assert response.json() == {
            'uuid': str(next_list.uuid),
            'name': 'nexts',
            'next': {
                'score': 6,
                'weight': 0,
                'origin': Item.Origin.NEXT,
                'resource': {
                    'uuid': str(rc.uuid),
                    'name': 'C',
                    'year': 2020,
                    'poster_path': '/c.jpg',
                    'viewing_status': Viewing.Status.NOTSEEN,
                }
            },
            'nexts': [
                {
                    'score': 6,
                    'weight': 0,
                    'origin': Item.Origin.NEXT,
                    'resource': {
                        'uuid': str(rc.uuid),
                        'name': 'C',
                        'year': 2020,
                        'poster_path': '/c.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 4,
                    'weight': 0,
                    'origin': Item.Origin.NEXT,
                    'resource': {
                        'uuid': str(rd.uuid),
                        'name': 'D',
                        'year': 2020,
                        'poster_path': '/d.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 2,
                    'weight': 0,
                    'origin': Item.Origin.NEXT,
                    'resource': {
                        'uuid': str(Resource.objects.get(name='E').uuid),
                        'name': 'E',
                        'year': 2020,
                        'poster_path': '/e.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
            ],
            'picks': [
                {
                    'score': 5,
                    'weight': 4,
                    'origin': Item.Origin.PICK,
                    'resource': {
                        'uuid': str(ra.uuid),
                        'name': 'A',
                        'year': 2020,
                        'poster_path': '/a.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 4,
                    'weight': 2,
                    'origin': Item.Origin.PICK,
                    'resource': {
                        'uuid': str(rb.uuid),
                        'name': 'B',
                        'year': 2020,
                        'poster_path': '/b.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 4,
                    'weight': 2,
                    'origin': Item.Origin.PICK,
                    'resource': {
                        'uuid': str(rh.uuid),
                        'name': 'H',
                        'year': 2020,
                        'poster_path': '/h.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 0,
                    'weight': 1,
                    'origin': Item.Origin.PICK,
                    'resource': {
                        'uuid': str(rg.uuid),
                        'name': 'G',
                        'year': 2020,
                        'poster_path': '/g.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
            ],
        }

    @pytest.mark.parametrize('data', [{}, {'uuids': [None, 1, 'foo']}])
    def test_invalid_data(self, user, auth_client, data):
        empty_list = NextListFactory(name='empty')
        url = f'/watchnext/next_lists/{empty_list.uuid}/pick_resource/'
        response = auth_client.post(url, data=data)

        assert response.status_code == 400, response.json()


@pytest.mark.django_db
class TestNextListViewSetPickEnjoyed():

    def test_anonymous_user(self):
        next_list = NextListFactory()
        url = f'/watchnext/next_lists/{next_list.uuid}/pick_enjoyed/'
        data = {'uuids': [uuid.uuid4()]}
        response = APIClient().post(url, data=data)

        assert response.status_code == 403

    def test_other_user(self, auth_client):
        other_user = UserFactory(username='other')
        next_list = NextListFactory(user=other_user)
        url = f'/watchnext/next_lists/{next_list.uuid}/pick_enjoyed/'
        data = {'uuids': [uuid.uuid4()]}
        response = auth_client.post(url, data=data)

        assert response.status_code == 404

    @responses.activate
    def test_with_enjoyed_resources(self, auth_client, tmdb_url):
        next_list = NextListFactory()
        ra = ResourceFactory(name='A')
        rb = ResourceFactory(name='B')
        rc = ResourceFactory(name='C')
        rd = ResourceFactory(name='D')
        rg = ResourceFactory(name='G')
        rh = ResourceFactory(name='H')
        ri = ResourceFactory(name='I')
        ra.recommendations.add(rb, rc, rd, rh)
        rg.recommendations.add(ra)
        rh.recommendations.add(ra)
        ri.recommendations.add(ra)
        url = tmdb_url(f'/movie/{rb.tmdb_id}/recommendations', '&page=1')
        json = {
            "page": 1,
            "results": [
                {
                    "genre_ids": [18],
                    "original_language": "en",
                    "title": "A",
                    "poster_path": "/a.jpg",
                    "release_date": "2020-09-22",
                    "id": ra.tmdb_id,
                },
                {
                    "genre_ids": [18],
                    "original_language": "en",
                    "title": "C",
                    "poster_path": "/c.jpg",
                    "release_date": "2020-09-22",
                    "id": rc.tmdb_id,
                },
                {
                    "genre_ids": [18],
                    "original_language": "en",
                    "title": "E",
                    "poster_path": "/e.jpg",
                    "release_date": "2020-09-22",
                    "id": 101,
                },
            ],
            "total_pages": 1,
            "total_results": 3
        }
        responses.add('GET', url, json=json)
        for resource in [ra, rb, rg, rh]:
            ViewingFactory(resource=resource)
        url = f'/watchnext/next_lists/{next_list.uuid}/pick_enjoyed/'
        response = auth_client.post(url)

        assert response.status_code == 200
        assert response.json() == {
            'uuid': str(next_list.uuid),
            'name': 'nexts',
            'next': {
                'score': 6,
                'weight': 0,
                'origin': Item.Origin.NEXT,
                'resource': {
                    'uuid': str(rc.uuid),
                    'name': 'C',
                    'year': 2020,
                    'poster_path': '/c.jpg',
                    'viewing_status': Viewing.Status.NOTSEEN,
                }
            },
            'nexts': [
                {
                    'score': 6,
                    'weight': 0,
                    'origin': Item.Origin.NEXT,
                    'resource': {
                        'uuid': str(rc.uuid),
                        'name': 'C',
                        'year': 2020,
                        'poster_path': '/c.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 4,
                    'weight': 0,
                    'origin': Item.Origin.NEXT,
                    'resource': {
                        'uuid': str(rd.uuid),
                        'name': 'D',
                        'year': 2020,
                        'poster_path': '/d.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 2,
                    'weight': 0,
                    'origin': Item.Origin.NEXT,
                    'resource': {
                        'uuid': str(Resource.objects.get(name='E').uuid),
                        'name': 'E',
                        'year': 2020,
                        'poster_path': '/e.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
            ],
            'picks': [
                {
                    'score': 5,
                    'weight': 4,
                    'origin': Item.Origin.PICK,
                    'resource': {
                        'uuid': str(ra.uuid),
                        'name': 'A',
                        'year': 2020,
                        'poster_path': '/a.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 4,
                    'weight': 2,
                    'origin': Item.Origin.PICK,
                    'resource': {
                        'uuid': str(rb.uuid),
                        'name': 'B',
                        'year': 2020,
                        'poster_path': '/b.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 4,
                    'weight': 2,
                    'origin': Item.Origin.PICK,
                    'resource': {
                        'uuid': str(rh.uuid),
                        'name': 'H',
                        'year': 2020,
                        'poster_path': '/h.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
                {
                    'score': 0,
                    'weight': 1,
                    'origin': Item.Origin.PICK,
                    'resource': {
                        'uuid': str(rg.uuid),
                        'name': 'G',
                        'year': 2020,
                        'poster_path': '/g.jpg',
                        'viewing_status': Viewing.Status.NOTSEEN,
                    }
                },
            ],
        }

    def test_without_enjoyed_resources(self, user, auth_client):
        empty_list = NextListFactory(name='empty')
        url = f'/watchnext/next_lists/{empty_list.uuid}/pick_enjoyed/'
        response = auth_client.post(url)

        assert response.status_code == 200
        assert response.json() == {
            'uuid': str(empty_list.uuid),
            'name': 'empty',
            'next': None,
            'nexts': [],
            'picks': [],
        }


@pytest.mark.django_db
class TestImportImdb():
    url = '/watchnext/imports/imdb/'

    @responses.activate
    def test_resources_do_not_exist(
        self, movie_find_request, serie_find_request,
    ):
        movie_url, movie_json = movie_find_request
        serie_url, serie_json = serie_find_request
        responses.add('GET', movie_url, json=movie_json)
        responses.add('GET', serie_url, json=serie_json)

        data = {'imdb_ids': ['tt0137523', 'tt2442560']}
        response = APIClient().post(self.url, data=data)

        assert response.status_code == 201
        assert len(responses.calls) == 2
        assert Resource.objects.count() == 2

        movie = Resource.objects.get(imdb_id='tt0137523')
        assert movie.tmdb_id == 550
        assert movie.name == 'Fight Club'
        assert movie.lang == 'en'
        assert movie.date == datetime.date(1999, 10, 15)
        assert movie.kind == Resource.Kind.MOVIE
        assert movie.poster_path == '/8kNruSfhk5IoE4eZOc4UpvDn6tq.jpg'

        serie = Resource.objects.get(imdb_id='tt2442560')
        assert serie.tmdb_id == 550
        assert serie.name == 'Peaky Blinders'
        assert serie.lang == 'en'
        assert serie.date == datetime.date(2013, 9, 12)
        assert serie.kind == Resource.Kind.SERIE
        assert serie.poster_path == '/6PX0r5TRRU5y0jZ70y1OtbLYmoD.jpg'

    @responses.activate
    def test_resources_exist(
        self, movie_find_request, serie_find_request,
        movie_resource, serie_resource,
    ):
        movie_url, movie_json = movie_find_request
        serie_url, serie_json = serie_find_request
        responses.add('GET', movie_url, json=movie_json)
        responses.add('GET', serie_url, json=serie_json)

        data = {'imdb_ids': ['tt0137523', 'tt2442560']}
        response = APIClient().post(self.url, data=data)

        assert response.status_code == 201
        assert len(responses.calls) == 1
        assert Resource.objects.count() == 2

        movie = Resource.objects.get(imdb_id='tt0137523')
        serie = Resource.objects.get(imdb_id='tt2442560')
        assert movie == movie_resource
        assert serie == serie_resource

    @pytest.mark.parametrize('data', [
        {},
        {'fail_ids': ['tt0137523', 'tt2442560']},
    ])
    def test_invalid_data(self, data):
        response = APIClient().post(self.url, data=data)

        assert response.status_code == 400


@pytest.mark.django_db
class TestImportTMDb():
    url = '/watchnext/imports/tmdb/'

    @responses.activate
    def test_resources_do_not_exist(self, movie_details_request):
        movie_url, movie_json = movie_details_request
        responses.add('GET', movie_url, json=movie_json)

        data = {'tmdb_ids': [550], 'kind': Resource.Kind.MOVIE}
        response = APIClient().post(self.url, data=data)

        assert response.status_code == 201
        assert len(responses.calls) == 1
        assert Resource.objects.count() == 1

        movie = Resource.objects.get(tmdb_id=550)
        assert movie.imdb_id == 'tt0137523'
        assert movie.name == 'Fight Club'
        assert movie.lang == 'en'
        assert movie.date == datetime.date(1999, 10, 15)
        assert movie.kind == Resource.Kind.MOVIE
        assert movie.poster_path == '/8kNruSfhk5IoE4eZOc4UpvDn6tq.jpg'

    @responses.activate
    def test_resources_exist(self, serie_details_request, serie_resource):
        serie_url, serie_json = serie_details_request
        responses.add('GET', serie_url, json=serie_json)

        data = {'tmdb_ids': [550], 'kind': Resource.Kind.SERIE}
        response = APIClient().post(self.url, data=data)

        assert response.status_code == 201
        assert len(responses.calls) == 0
        assert Resource.objects.count() == 1

        serie = Resource.objects.get(tmdb_id=550)
        assert serie == serie_resource

    @pytest.mark.parametrize('data', [
        {},
        {'fail_ids': [550]},
    ])
    def test_invalid_data(self, data):
        response = APIClient().post(self.url, data=data)

        assert response.status_code == 400
