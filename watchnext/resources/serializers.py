from rest_framework import serializers
from rest_framework import validators

from watchnext.resources.mixins import ViewingStatusMixin
from watchnext.resources.models import (
    Genre, Item, NextList, Resource, Viewing,
)


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ['id', 'name']


class ResourceListSerializer(ViewingStatusMixin, serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = ['uuid', 'name', 'year', 'poster_path']


class ResourceSerializer(ViewingStatusMixin, serializers.ModelSerializer):
    genres = GenreSerializer(many=True, read_only=True)
    recommended_by = ResourceListSerializer(many=True, read_only=True)
    recommendations = ResourceListSerializer(many=True, read_only=True)

    class Meta:
        model = Resource
        fields = [
            'uuid', 'name', 'date', 'poster_path',
            'genres', 'recommended_by', 'recommendations',
        ]
        list_serializer_class = ResourceListSerializer


class ViewingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Viewing
        fields = ['user', 'resource', 'status']


class ItemSerializer(serializers.ModelSerializer):
    resource = ResourceListSerializer(read_only=True)

    class Meta:
        model = Item
        fields = ['origin', 'weight', 'score', 'resource']


class NextListListSerializer(serializers.ModelSerializer):
    next = ItemSerializer(read_only=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = NextList
        fields = ['uuid', 'name', 'next', 'user']


class NextListSerializer(serializers.ModelSerializer):
    next = ItemSerializer(read_only=True)
    nexts = ItemSerializer(many=True, read_only=True)
    picks = ItemSerializer(many=True, read_only=True)

    class Meta:
        model = NextList
        fields = ['uuid', 'name', 'next', 'nexts', 'picks']


class ImdbSerializer(serializers.Serializer):
    imdb_ids = serializers.ListField(child=serializers.CharField())


class TMDbSerializer(serializers.Serializer):
    tmdb_ids = serializers.ListField(child=serializers.IntegerField())
    kind = serializers.ChoiceField(choices=Resource.Kind.choices)


class ResourceRetrieverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = [
            'tmdb_id', 'imdb_id', 'name', 'lang',
            'date', 'kind', 'poster_path', 'genres',
        ]
        validators = [
            validators.UniqueTogetherValidator(
                queryset=Resource.objects.all(),
                fields=['tmdb_id', 'kind'],
            )
        ]

    def to_internal_value(self, data):
        name_key, date_key = {
            Resource.Kind.MOVIE: ('title', 'release_date'),
            Resource.Kind.SERIE: ('name', 'first_air_date'),
        }[data.get('kind', Resource.Kind.MOVIE)]

        genres_tmdb_ids = data.get(
            'genre_ids',
            [
                genre['id']
                for genre in data.get('genres', [])
            ],
        )
        genres = Genre.objects.filter(tmdb_id__in=genres_tmdb_ids)

        return super().to_internal_value({
            'tmdb_id': data.get('id'),
            'imdb_id': data.get('imdb_id'),
            'name': data.get(name_key),
            'lang': data.get('original_language'),
            'date': data.get(date_key),
            'kind': data.get('kind'),
            'poster_path': data.get('poster_path'),
            'genres': [genre.id for genre in genres],
        })


class UUIDListSerializer(serializers.Serializer):
    uuids = serializers.ListField(child=serializers.UUIDField())
