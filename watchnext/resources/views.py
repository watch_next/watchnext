from rest_framework import generics, mixins, permissions, viewsets
from rest_framework.decorators import action, api_view
from rest_framework.response import Response

from watchnext.resources.models import NextList, Resource, Viewing
from watchnext.resources.serializers import (
    ResourceSerializer, ResourceListSerializer,
    NextListSerializer, NextListListSerializer,
    ImdbSerializer, TMDbSerializer, ViewingSerializer,
    UUIDListSerializer,
)
from watchnext.resources.utils import NextListReloader, ResourceRetriever


class MovieList(generics.ListAPIView):
    queryset = Resource.objects.filter(kind=Resource.Kind.MOVIE)
    serializer_class = ResourceListSerializer


class SerieList(generics.ListAPIView):
    queryset = Resource.objects.filter(kind=Resource.Kind.SERIE)
    serializer_class = ResourceListSerializer


class ResourceViewSet(
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'resource_uuid'

    @action(detail=True, methods=['POST'], permission_classes=[permissions.IsAuthenticated])
    def viewing(self, request, resource_uuid=None):
        instance = Viewing.objects.get_or_none(
            user=request.user, resource__uuid=resource_uuid,
        )
        data = {
            'user': request.user.uuid,
            'resource': resource_uuid,
            **request.data,
        }
        serializer = ViewingSerializer(instance, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=200)
        else:
            return Response(data=serializer.errors, status=400)


class NextListViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = 'uuid'
    lookup_url_kwarg = 'next_list_uuid'

    def get_queryset(self):
        return self.request.user.next_lists.all()

    def get_serializer_class(self):
        if self.action in ('list', 'create'):
            return NextListListSerializer
        else:
            return NextListSerializer

    @action(detail=True, methods=['POST'])
    def pick_resource(self, request, next_list_uuid=None):
        next_list = NextList.objects.get_or_none(
            uuid=next_list_uuid, user=self.request.user,
        )
        if not next_list:
            return Response(status=404)

        serializer = UUIDListSerializer(data=request.data)
        if serializer.is_valid():
            uuids = serializer.validated_data['uuids']
            picks = Resource.objects.filter(uuid__in=uuids)
            NextListReloader.picks(next_list, picks)
            NextListReloader.nexts(next_list)
            data = NextListSerializer(next_list).data
            return Response(data=data, status=200)
        else:
            return Response(data=serializer.errors, status=400)

    @action(detail=True, methods=['POST'])
    def pick_enjoyed(self, request, next_list_uuid=None):
        next_list = NextList.objects.get_or_none(
            uuid=next_list_uuid, user=self.request.user,
        )
        if next_list:
            picks = Resource.objects.filter(
                viewings__user=request.user, viewings__status=Viewing.Status.ENJOYED,
            )
            NextListReloader.picks(next_list, picks)
            NextListReloader.nexts(next_list)
            data = NextListSerializer(next_list).data
            return Response(data=data, status=200)
        else:
            return Response(status=404)


@api_view(['POST'])
def import_imdb(request):
    serializer = ImdbSerializer(data=request.data)
    if serializer.is_valid():
        for id in serializer.validated_data['imdb_ids']:
            ResourceRetriever.with_imdb_id(id)
        return Response(status=201)
    else:
        return Response(data=serializer.errors, status=400)


@api_view(['POST'])
def import_tmdb(request):
    serializer = TMDbSerializer(data=request.data)
    if serializer.is_valid():
        kind = serializer.validated_data['kind']
        for id in serializer.validated_data['tmdb_ids']:
            ResourceRetriever.with_tmdb_id_and_kind(id, kind)
        return Response(status=201)
    else:
        return Response(data=serializer.errors, status=400)
