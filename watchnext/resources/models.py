from uuid import uuid4

from django.conf import settings
from django.db import models


class GetOrNoneManager(models.Manager):

    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None


class Genre(models.Model):
    name = models.CharField(max_length=32)
    tmdb_id = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class Resource(models.Model):

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['tmdb_id', 'kind'],
                name='tmdb_resource',
            )
        ]
        ordering = ['name']

    class Kind(models.IntegerChoices):
        MOVIE = 1
        SERIE = 2

    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    tmdb_id = models.PositiveBigIntegerField()
    imdb_id = models.CharField(max_length=32, null=True, blank=True)

    name = models.CharField(max_length=256)
    lang = models.CharField(max_length=8)
    date = models.DateField()
    kind = models.IntegerField(choices=Kind.choices)
    poster_path = models.CharField(max_length=128, null=True, blank=True)

    genres = models.ManyToManyField(
        'Genre',
        related_name='resources',
    )
    recommendations = models.ManyToManyField(
        'Resource',
        related_name='recommended_by',
    )
    viewers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='views',
        through='Viewing',
    )

    objects = GetOrNoneManager()

    def __str__(self):
        return self.name

    @property
    def year(self):
        return self.date.year

    @property
    def file_name(self):
        return f"{self.name.replace(':', ' -')} ({self.year})"


class Viewing(models.Model):

    class Status(models.IntegerChoices):
        ENJOYED = 1
        UNLIKED = 2
        NEITHER = 3
        NOTSEEN = 4

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='viewings',
        on_delete=models.CASCADE,
    )
    resource = models.ForeignKey(
        'Resource',
        related_name='viewings',
        on_delete=models.CASCADE,
    )
    status = models.IntegerField(choices=Status.choices)

    objects = GetOrNoneManager()

    def __str__(self):
        return self.resource.name


# class NextListManager(models.Manager):

#     def reload_picks(self):
#         for pick in self.model.picks:
#             if not pick.


class NextList(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=256)
    # logo = models.ImageField()

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='next_lists',
        on_delete=models.CASCADE,
    )
    resources = models.ManyToManyField(
        'Resource',
        related_name='next_lists',
        through='Item',
    )

    objects = GetOrNoneManager()

    @property
    def picks(self):
        return self.items \
            .filter(origin=Item.Origin.PICK) \
            .order_by('-weight')

    @property
    def nexts(self):
        return self.items \
            .filter(origin=Item.Origin.NEXT) \
            .order_by('-score')

    @property
    def next(self):
        return self.nexts.first()

    def __str__(self):
        return self.name


class Item(models.Model):

    class Origin(models.IntegerChoices):
        PICK = 1
        NEXT = 2

    resource = models.ForeignKey(
        'Resource',
        related_name='items',
        on_delete=models.CASCADE,
    )
    next_list = models.ForeignKey(
        'NextList',
        related_name='items',
        on_delete=models.CASCADE,
    )
    origin = models.IntegerField(choices=Origin.choices)
    weight = models.IntegerField(default=0)
    score = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.resource} in {self.next_list}'
