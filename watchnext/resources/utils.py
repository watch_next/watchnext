from typing import Any, Dict, List, Optional

import requests
from django.conf import settings

from watchnext.resources.models import Item, Resource
from watchnext.resources.serializers import ResourceRetrieverSerializer


RESOURCE_KIND_URL = {
    Resource.Kind.MOVIE: '/movie',
    Resource.Kind.SERIE: '/tv',
}


class TMDbClient(object):

    @staticmethod
    def get_data(url: str, kind: Optional[Resource.Kind] = None, **params) -> Optional[Dict[str, Any]]:
        if kind:
            url = f'{RESOURCE_KIND_URL[kind]}{url}'
        url = f'{settings.BASE_URL}{url}'
        params['api_key'] = settings.API_KEY
        if (response := requests.get(url, params)).status_code == 200:
            return response.json()

    @staticmethod
    def get_paginated_data(url: str, page: int = 1, **params) -> List[Dict[str, Any]]:
        data_list, more_pages = [], True
        while more_pages:
            if (paginated_data := TMDbClient.get_data(url, page=page, **params)):
                data_list.extend(paginated_data['results'])
                more_pages = page < paginated_data['total_pages']
                page += 1
        return data_list


class ResourceRetriever(object):

    @staticmethod
    def with_tmdb_id_and_kind(tmdb_id: int, kind: Resource.Kind) -> Optional[Resource]:
        if (resource := Resource.objects.get_or_none(tmdb_id=tmdb_id, kind=kind)):
            return resource

        url, params = f'/{tmdb_id}', {'kind': kind}
        if (data := TMDbClient.get_data(url, **params)):
            serializer = ResourceRetrieverSerializer(data={**data, 'kind': kind})
            if serializer.is_valid():
                return serializer.save()

    @staticmethod
    def with_imdb_id(imdb_id: str) -> Optional[Resource]:
        if (resource := Resource.objects.get_or_none(imdb_id=imdb_id)):
            return resource

        url, params = f'/find/{imdb_id}', {'external_source': 'imdb_id'}
        if (data := TMDbClient.get_data(url, **params)):
            if (resource_data := (data['movie_results'] and data['movie_results'][0])):
                resource_data['kind'] = Resource.Kind.MOVIE
            elif (resource_data := (data['tv_results'] and data['tv_results'][0])):
                resource_data['kind'] = Resource.Kind.SERIE
            else:
                return None

            resource = Resource.objects.get_or_none(
                tmdb_id=resource_data['id'],
                kind=resource_data['kind'],
            )
            data = {**resource_data, 'imdb_id': imdb_id}
            serializer = ResourceRetrieverSerializer(resource, data=data)
            if serializer.is_valid():
                return serializer.save()

    @staticmethod
    def recommendations(resource):
        url = f'/{resource.tmdb_id}/recommendations'
        for data in TMDbClient.get_paginated_data(url, kind=resource.kind):
            params = {'tmdb_id': data['id'], 'kind': resource.kind}
            instance = Resource.objects.get_or_none(**params)
            serializer = ResourceRetrieverSerializer(
                instance, data={**data, 'kind': resource.kind}
            )
            if serializer.is_valid():
                resource.recommendations.add(serializer.save())

        resource.save()
        return resource


class NextListReloader():

    @staticmethod
    def picks(next_list, picks):
        to_create = picks.exclude(items__in=next_list.items.all())
        to_update = next_list.nexts.filter(resource__in=picks)

        for resource in to_create:
            Item.objects.create(
                resource=resource, next_list=next_list,
                origin=Item.Origin.PICK,
            )

        for item in to_update:
            item.origin = Item.Origin.PICK
            item.save()

        return next_list

    @staticmethod
    def nexts(next_list):
        for pick in next_list.picks:
            if not pick.resource.recommendations.count():
                ResourceRetriever.recommendations(pick.resource)

        for pick in next_list.picks:
            pick.weight = 1 + next_list.picks.filter(
                resource__in=pick.resource.recommended_by.all()
            ).count()
            pick.save()

        for pick in next_list.picks:
            pick.score = sum(
                item.weight
                for item in next_list.picks.filter(
                    resource__in=pick.resource.recommended_by.all()
                )
            )
            pick.save()

            recommendations = pick.resource.recommendations.exclude(
                items__in=next_list.items.all()
            )
            for recommendation in recommendations:
                Item.objects.create(
                    resource=recommendation, next_list=next_list,
                    origin=Item.Origin.NEXT,
                )

        for nxt in next_list.nexts:
            nxt.weight = 0
            nxt.score = sum(
                item.weight
                for item in next_list.picks.filter(
                    resource__in=nxt.resource.recommended_by.all()
                )
            )
            nxt.save()
