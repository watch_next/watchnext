from watchnext.resources.models import Viewing


class ViewingStatusMixin(object):

    def to_representation(self, instance):
        if (
            (request := self.context.get('request')) and
            request.user.is_authenticated and
            (viewing := instance.viewings.get_or_none(user=request.user))
        ):
            viewing_status = viewing.status
        else:
            viewing_status = Viewing.Status.NOTSEEN

        return {
            **super().to_representation(instance),
            'viewing_status': viewing_status,
        }
