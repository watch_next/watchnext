from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'watchnext.users'
