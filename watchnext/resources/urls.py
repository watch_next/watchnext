from django.urls import path, include
from rest_framework.routers import DefaultRouter

from watchnext.resources import views

router = DefaultRouter()
router.register(r'resources', views.ResourceViewSet, basename='resources')
router.register(r'next_lists', views.NextListViewSet, basename='next_list')
urlpatterns = [
    path('', include(router.urls)),
    path('movies/', views.MovieList.as_view()),
    path('series/', views.SerieList.as_view()),
    path('imports/imdb/', views.import_imdb),
    path('imports/tmdb/', views.import_tmdb),
]
